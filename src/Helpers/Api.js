import axios from 'axios'

let requestAxios = axios.create({
  baseURL: 'http://trends-playground.myntra.com/myntra-trends-service',
});

export function fetchFeedData(currentPage, fetchSize) {
  return (
    requestAxios({
      method:`post`,
      url: `/tagging/filter?fetchSize=${fetchSize}&start=${currentPage}`,
      data: { filters: [{"season": "spring-summer-2019"}] }
    }).catch(error => {
      return Promise.reject(new Error(error.message))
    })
  )
}

export function updateFeedData(payload) {
	return (
    requestAxios({
      method:`post`,
      headers: {
        'Content-Type': 'application/json'
      },
      url:`/tagging/tag`,
      data: payload
    }).catch(error => {
      return Promise.reject(new Error(error.message))
    })
  )
}
