import { action } from '../utils/helpers'

export const fetchFeed = (payload) => action('FETCH_FEED', payload)
export const updateTag = (payload) => action('UPDATE_TAG', payload)
