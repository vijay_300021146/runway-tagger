import React, { Component } from 'react'
import { Field, reduxForm, submit } from 'redux-form'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import FeedSelect from '../../Components/Feed/FeedSelect'
import { ARTICLE_TYPES } from '../../utils/constants'
import { updateTag } from '../../actions/home'
import store from '../../store'

const onSubmit = (values, dispatch, props) => {
  try {
    const tags = []
    Object.keys(values).map(item => {
      tags.push({
        tag: item,
        article_type: values[item]
      })
      return null
    })
    let payload = {
      id: props.id,
      tags
    }

    if (props.autoSubmit > 0) {
      props.updateTag({ body: payload, id: props.id })
    }
  }
  catch(e) {
    console.log('onSubmiterror: ', e)
  }
}


class FeedDetailsForm extends Component {
  static propTypes = {
    tagData: PropTypes.array.isRequired,
    initialValues: PropTypes.object.isRequired,
    image: PropTypes.string.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      tag: '',
      error: null
    }
  }

  componentDidMount() {
    store.dispatch(submit(this.props.form))
  }

  handleOnChange = (e) => {
    const { value, name } = e.target
    let payload = {
      id: this.props.id,
      tags: [
        {
          tag: name ? name : '',
          article_type: value ? value : ''
        }
      ]
    }
    this.setState({tag: '', error: null})
    this.props.updateTag({ body: payload, name, id: this.props.id })
    .then((res) => {
      console.log(`submit ${name} succeeded'`)
    }, (error) => {
      this.setState({tag: name, error: error.toString()})
    })
    .catch(error => {
      this.setState({tag: name, error: error.toString()})
      console.log('error: ', error.toString())
    })
  }


  render() {
    const { tagData, handleSubmit, form, list } = this.props
    const { tag } = this.state
    return (
      <div className={"feedDetailsForm"}>
        <form id={"feedDetailsForm"} onSubmit={handleSubmit(onSubmit)}>
          {
            tagData.map(item =>
              <Field name={item.tag} key={item.tag} errorField={item.tag === tag} error={list.error} component={FeedSelect} options={ARTICLE_TYPES} type="text"  onChange={this.handleOnChange}/>
            )
          }
        </form>

      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  const { update } = state
  return {
    ...props.initialValues,
    list: update.list[props.id] || {}
  }
}


var Form = reduxForm({
  getFormState: state => state.form,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  destroyOnUnmount: false,
  onSubmit
})(FeedDetailsForm)

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateTag
    },
    dispatch
  )

export default connect(mapStateToProps, mapDispatchToProps)(Form)
