import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FeedMainComponent } from '../../Components/componentIndex'
import { fetchFeed } from '../../actions/home'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Pagination from '../Pagination'
import FeedLoading from '../../Components/Feed/FeedLoading'
import { push } from '../../utils/helpers'
import { isEmpty } from '../../utils/validations'
import { fetchFeedData, updateFeedData } from '../../Helpers/Api'
import { ARTICLE_TYPES } from '../../utils/constants'

class MainAppContainer extends Component {
  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
    feedData: PropTypes.array.isRequired,
    fetchSize: PropTypes.number.isRequired,
    currentPage:PropTypes.number.isRequired,
    totalCount:PropTypes.number.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      imageUrl: ''
    }
  }

  componentDidMount() {
    const { query } = this.props
    const currentPage = query.page ? parseInt(query.page - 1, 10) : (this.props.currentPage - 1)
    const fetchSize = query.fetchSize ? parseInt(query.fetchSize, 10) : this.props.fetchSize
    this.loadFeedData(currentPage, fetchSize)
  }

  loadFeedData = (currentPage, fetchSize) => {
    this.props.fetchFeed({ start: currentPage * fetchSize, fetchSize, currentPage, body: { filters: [{"season": "spring-summer-2019"}
    ] } })
    // this.autoTag(0, 8974)
  }

  autoTag = (start, fetchSize) => {
    fetchFeedData(start, fetchSize)
    .then(res => {
      console.log('res: ', res.data.data)
      const feedData = res.data.data
      // eslint-disable-next-line
      feedData.map(item1 => {
        let initialValues = {}
        let tags = []
        let autoSubmit = 0
        try {
          item1.tags.map(( item) => {
            // eslint-disable-next-line
            const list = ARTICLE_TYPES.find((list) => {
                const alt1 = new RegExp(`${list.alt1}`, 'gi')
                const alt2 = new RegExp(`${list.alt2}`, 'gi')
                if (item.tag.search(alt1) > -1 || item.tag.search(alt2) > -1) {
                  return list
                }
              })
            if (list && list.value) {
              initialValues[item.tag] = list.value
              tags.push({tag: item.tag, article_type: list.value})
              autoSubmit += 1
            }
            if (item.article_type) {
              initialValues[item.tag] = item.article_type
              autoSubmit += -1
            }
            return null
          })
        } catch(e) {
          console.log('auto: ', e)
        }
          if (autoSubmit > 0) {
            updateFeedData({id: item1.id, tags})
            // console.log('tags: ', tags)
          }
      })
    }, error => {
      console.log('error: ', error)
    })
    .catch(err => {
      console.log('error: ', err)
    })
  }

  onPageChange = (page, { sizePerPage }) => {
    this.loadFeedData(page - 1, sizePerPage)
    const query = {page, fetchSize: sizePerPage}
    push(this.props.pathType, query)
  }

  modalOpen = (imageUrl) => {
    this.setState({
      isOpen: true,
      imageUrl
    })
  }

  modalClose = () => {
    this.setState({
      isOpen: false,
      imageUrl: ''
    })
  }

  render() {
    const {isLoading, feedData, error } = this.props;
    return (
      <div>
        {
          this.state.isOpen &&
          <div className={"modalContainer"} onClick={this.modalClose}>
            <div className={"modalContainerInner"}>
              <img src={this.state.imageUrl} alt={this.state.imageUrl} />
            </div>
          </div>
        }
        {
          isLoading &&
          <FeedLoading />
        }
        {
          !isLoading && isEmpty(error) &&
          <FeedMainComponent 
            feedData = {feedData}
            modalOpen= {this.modalOpen} />
        }
        {
          !isLoading && !isEmpty(error) &&
          <div className={"formError"}>{error.toString()}</div>
        }
        <div className={"paginationContainer"}>
          <Pagination 
            currentPage={this.props.currentPage}
            totalSize={this.props.totalCount}
            onChange={this.onPageChange}
            sizePerPage={this.props.fetchSize}
            sizes={[45, 90]} />
        </div>
      </div>
    )
  }

}

const mapStateToProps = state => {
  const { home, router } = state
  return {
    ...home,
    pathType: router.type,
    query: router.query || {}
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchFeed
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainAppContainer)
