import { fetch } from '../api'
// import { isEmpty } from 'lodash'

export const home = store => next => action => {
    const result = next(action)
    const { type, payload } = action
    // const state = store.getState()

    // const loginRedirect = (token, path) => {
    //     if (token.length === 0) { return null }

    //     setCookie('authToken', token, 1)
    //     store.dispatch(push(path))        
    // }

    switch( type ) {
        case 'FETCH_FEED': {
            // TODO: duplicate user error
            const { fetchSize, start, currentPage, body } = payload
            fetch(`/tagging/filter?fetchSize=${fetchSize}&start=${start}`, 'POST', { data: body })
            .then((res) => {
                store.dispatch({ type: type + '_SUCCESS', payload: { feedData: res.data.data, currentPage: currentPage + 1, fetchSize, totalCount: res.data.status.total_count, isLoading: false } })
            }, error => {
                store.dispatch({ type: type + '_ERROR', payload: { error } })
            })
            .catch((error) => {
                store.dispatch({ type: type + '_ERROR', payload: { error } })
            })
        }
        break
        case 'UPDATE_TAG': {
            const { body, id } = payload
            return fetch('/tagging/tag', 'POST', { data: body })
            .then((res) => {
                store.dispatch({ type: type + '_SUCCESS', payload: { ...res.data, id } })
                return Promise.resolve(res)
            }, error => {
                store.dispatch({ type: type + '_ERROR', payload: { error: error.toString(), id } })
                return Promise.reject(error)
            })
            .catch((error) => {
                store.dispatch({ type: type + '_ERROR', payload: { error: error.toString(), id } })
                return Promise.reject(error)
            })
        }// eslint-disable-next-line
        break
        default:
           return result; 
    }
}
