import React from 'react'
import PropTypes from 'prop-types'
import { isEmpty } from '../../utils/validations'

const FeedSelect = (props) => {
  const { name, value } = props.input
  const { errorField, ...rest } = props

  return (
    <div className={'feedSelectWrapper'}>
      <div className={"feedSelect"}>
        <label className={"feedSelectLabel"} title={name}>{name}</label>
        <div className={"selectBoxWrapper"}>
          <input {...props.input} {...rest} />
          <select {...props.input} className={"feedSelectBox"} {...rest} tabIndex={!isEmpty(value.trim()) ? -1 : 0}>
            <option />
            {
              props.options.map(item =>
                <option value={item.value} key={item.alt1}>{item.value}</option>
              )
            }
          </select>
        </div>
      </div>
      {
        (errorField) ?
        <span className={"formError"}>{props.meta.error || props.error}</span> : null
      }
    </div>
  )
}

FeedSelect.propTypes = {
  input: PropTypes.object.isRequired,
  options: PropTypes.array.isRequired
}

export default FeedSelect
