import React from 'react';
import PropTypes from 'prop-types';
import FeedItem from './FeedItem';

const FeedMainComponent = (props) => {
  const { feedData } = props
  return (
    <div className={"feedMainContainer"}>
      {
        feedData.map((item, index) =>
          <FeedItem
            index={index}
            key={item.image}
            item={item}
            modalOpen={props.modalOpen} />
      )}
    </div>
  );
};

FeedMainComponent.propTypes = {
  feedData: PropTypes.array.isRequired,
  modalOpen: PropTypes.func.isRequired,
};

export default FeedMainComponent;
