import React from 'react';
import PropTypes from 'prop-types';
import FeedDetailsForm from '../../Containers/Feed/FeedDetailsForm';
import FeedImage from './FeedImage'
import { ARTICLE_TYPES } from '../../utils/constants'

const FeedItem = (props) => {
  const { item, index } = props
  let initialValues = {}
  let autoSubmit = 0
  try {
    item.tags.map(( item) => {
      // eslint-disable-next-line
      const list = ARTICLE_TYPES.find((list) => {
          const alt1 = new RegExp(`${list.alt1}`, 'gi')
          const alt2 = new RegExp(`${list.alt2}`, 'gi')
          if (item.tag.search(alt1) > -1 || item.tag.search(alt2) > -1) {
            return list
          }
        })
      if (list && list.value) {
        initialValues[item.tag] = list.value
        autoSubmit += 1
      }
      if (item.article_type) {
        initialValues[item.tag] = item.article_type
        autoSubmit += -1
      }
      return null
    })
  } catch(e) {
    console.log('set initialValues Error: ', e)
  }
  return (
    <div className={"feedItem"}>
      <FeedImage
        index={index}
        imageUrl={item.image}
        modalOpen={props.modalOpen} />
      <FeedDetailsForm
        form={`feedDetailsForm-${item.image}`}
        image={item.image}
        id={item.id}
        tagData={item.tags}
        initialValues={initialValues}
        autoSubmit={autoSubmit} />
    </div>
  );
};

FeedItem.propTypes = {
  item: PropTypes.object.isRequired,
  modalOpen: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired
};

export default FeedItem;
