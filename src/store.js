import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
// import { connectRouter, routerMiddleware } from 'connected-react-router';
import { connectRoutes } from 'redux-first-router';
import createHistory from 'history/createBrowserHistory';
import thunk from 'redux-thunk';
import reducers from './reducers';
import { reducer as form } from 'redux-form';
import { middlewares as globalMiddlewares } from './middlewares';
import queryString from 'query-string';
import routesMap from './routes';
import { createLogger } from 'redux-logger';

export const history = createHistory();

const { reducer, middleware, enhancer } = connectRoutes(routesMap, {
  location: 'router',
  querySerializer: queryString
})

const enhancers = [
  enhancer
];

const middlewares = [
  thunk,
  ...globalMiddlewares,
  middleware
];


const loggerMiddleware = createLogger({});

const initialState = {};




const rootReducer = combineReducers({ ...reducers, router: reducer, form })



if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

  middlewares.unshift(loggerMiddleware)
  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middlewares),
  ...enhancers,
);

const store = createStore(
  rootReducer,
  initialState,
  composedEnhancers
);

export default store;
