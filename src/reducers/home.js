const home = ( state = {
  isLoading: true,
  error: null,
  feedData: [],
  fetchSize: 90,
  currentPage: 1,
  totalCount: 1
}, { type, payload = {} }) => {
  switch (type) {
    case 'FETCH_FEED' :
      return {
        ...state,
        isLoading: true,
        error: null
      }
    case 'FETCH_FEED_SUCCESS' :
    case 'FETCH_FEED_ERROR' :
      return {
        ...state,
        isLoading: false,
        ...payload
      }
    default :
      return state;
  }
}

const update = ( state = {
  isLoading: true,
  isFetching: false,
  error: null,
  list: {}
}, { type, payload = {} }) => {
  switch (type) {
    case 'UPDATE_TAG' : {
      const { id } = payload
      return {
        ...state,
        list: {
          ...state.list,
          [id]: {
            ...state.list[id],
            isFetching: true,
            isLoading: true,
            error: null
          }
        }
      }
    }
    case 'UPDATE_TAG_SUCCESS' :
    case 'UPDATE_TAG_ERROR' : {
      const { id, ...rest } = payload
      return {
        ...state,
        list: {
          ...state.list,
          [id]: {
            ...state.list[id],
            isFetching: false,
            isLoading: false,
            ...rest
          }
        }
      }
    }
    default :
      return state;
  }
}

export default { home, update }
