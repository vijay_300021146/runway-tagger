export const ARTICLE_TYPES = [
  {alt2: 'dress', alt1: 'dress', label: 'Dress', value: 'Dress'},
  {alt2: 'shirt', alt1: 'shirt', label: 'Shirt', value: 'Shirt'},
  {alt2: 'tee-shirt', alt1: 'tshirt', label: 'TShirt', value: 'TShirt'},
  {alt2: 'pant', alt1: 'trouser', label: 'Trouser', value: 'Trouser'},
  {alt2: 'skirt', alt1: 'skirt', label: 'Skirt', value: 'Skirt'},
  {alt2: 'top', alt1: 'top', label: 'Top', value: 'Top'},
  {alt2: 'jean', alt1: 'jean', label: 'Jeans', value: 'Jeans'},
  {alt2: 'kurti', alt1: 'kurta', label: 'Kurta', value: 'Kurta'},
  {alt2: 'palazzo', alt1: 'palazzo', label: 'Palazzos', value: 'Palazzos'},
  {alt2: 'sweater', alt1: 'sweater', label: 'Sweater', value: 'Sweater'},
  {alt2: 'jacket', alt1: 'jacket', label: 'Jacket', value: 'Jacket'},
  {alt2: 'sweat shirt', alt1: 'sweatshirt', label: 'Sweatshirt', value: 'Sweatshirt'},
  {alt2: 'jumpsuit', alt1: 'jumpsuit', label: 'Jumpsuit', value: 'Jumpsuit'},
  {alt2: 'jumper', alt1: 'jumper', label: 'Sweater', value: 'Sweater'},
  {alt2: 'parka', alt1: 'parka', label: 'Jacket', value: 'Jacket'},
  {alt2: 'playsuit', alt1: 'playsuit', label: 'Jumpsuit', value: 'Jumpsuit'},
  {alt2: 'shoe', alt1: 'shoe', label: 'shoes', value: 'shoes'},
  {alt2: 'short', alt1: 'short', label: 'shorts', value: 'shorts'},
  {alt2: 'bag', alt1: 'bag', label: 'bag', value: 'bag'},
  {alt2: 'blazer', alt1: 'blazer', label: 'blazers', value: 'blazers'},
  {alt2: 'flat', alt1: 'flat', label: 'flats', value: 'flats'},
  {alt2: 'heel', alt1: 'heel', label: 'heels', value: 'heels'},
  {alt2: 'sunglass', alt1: 'sunglass', label: 'sunglasses', value: 'sunglasses'},
  {alt2: 'suit', alt1: 'suit', label: 'suit', value: 'suit'},
  {alt2: 'boxer', alt1: 'boxer', label: 'boxers', value: 'boxers'},
  {alt2: 'bra', alt1: 'bra', label: 'bra', value: 'bra'},
  {alt2: 'coat', alt1: 'coat', label: 'coats', value: 'coats'},
  {alt2: 'dungaree', alt1: 'dungaree', label: 'dungarees', value: 'dungarees'},
  {alt2: 'jewel', alt1: 'jewel', label: 'jewellery', value: 'jewellery'},
  {alt2: 'legging', alt1: 'legging', label: 'leggings', value: 'leggings'},
  {alt2: 'lingerie', alt1: 'lingerie', label: 'lingerie set', value: 'lingerie set'},
  {alt2: 'panties', alt1: 'panties', label: 'panties', value: 'panties'},
  {alt2: 'rain coat', alt1: 'raincoat', label: 'rain jacket', value: 'rain jacket'},
  {alt2: 'swim', alt1: 'swimsuit', label: 'swimwear', value: 'swimwear'},
  {alt2: 'track suit', alt1: 'tracksuit', label: 'tracksuits', value: 'tracksuits'},
  {alt2: 'waist coat', alt1: 'waistcoat', label: 'waistcoat', value: 'waistcoat'},
  {alt2: 'studs', alt1: 'earring', label: 'earring', value: 'earring'},
  {alt2: 'sweatpant', alt1: 'jogger', label: 'jogger', value: 'jogger'},
  {alt2: 'underpants', alt1: 'brief', label: 'briefs', value: 'briefs'}
]
