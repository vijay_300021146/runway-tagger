export const isEmpty = function(value) {
  return value === undefined || value === null || value === ''
}
